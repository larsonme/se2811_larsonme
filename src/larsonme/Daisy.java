/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;


import java.awt.*;

/**
 * A friendly flower providing nectar.
 */
public class Daisy extends Flower {
    /**
     * The amount of nectar to give the first AbstractBaseBee that visits this flower.
     */
    private static final int DELTA_NECTAR = 10;

    protected boolean hasNectar = true;

    /**
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public Daisy(Garden garden, Point location) {
        super(garden, location);
    }

    /**
     * Gives a AbstractBaseBee nectar, but only on its first visit
     * @param b The AbstractBaseBee to greet
     */
    @Override
    public void visitedBy(AbstractBaseBee b) {
        if (hasNectar)
            b.changeEnergy(DELTA_NECTAR);
        hasNectar = false;
    }
}
