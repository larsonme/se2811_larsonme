/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import java.awt.*;

/**
 * A dangerous flower that takes energy or kills bees that visit it.
 */
public class VenusBeeTrap extends Flower {
    public static final int VISIT_COST = 3;
    public boolean trippedTrap = false;

    /**
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public VenusBeeTrap(Garden garden, Point location) {
        super(garden, location);
    }


    /**
     * "Greets" the AbstractBaseBee by zapping some energy from it.
     * Once the flower has been visited once it is "tripped", and there is a
     * good chance it will simply kill the AbstractBaseBee.
     *
     * @param b The AbstractBaseBee to "greet"
     */
    @Override
    public void visitedBy(AbstractBaseBee b) {
        b.changeEnergy(-VISIT_COST);
    }
}
