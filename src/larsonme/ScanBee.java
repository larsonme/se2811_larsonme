/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import java.awt.*;

/** ScanBee moves top to bottom and then dies once it reaches the bottom right */
public class ScanBee extends AbstractBaseBee {
    private boolean goingRight = true;

    /**
     * @param garden Garden in which the AbstractBaseBee lives
     * @param startingLocation The AbstractBaseBee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The AbstractBaseBee's initial energy. Will be set to 0 if greater than 0.
     */
    public ScanBee(Garden garden, Point startingLocation, int initialEnergy) {
        super(garden, startingLocation, initialEnergy);
    }

    /**
     * Flies in a lawnmower pattern over the garden.
     *
     * Scans back and forth in horizontal lines.
     *
     * Dies upon reaching the bottom corner!
     */
    @Override
    public Flower step() {
        long x = Math.round(location.getX());
        long y = Math.round(location.getY());
        Flower flowerVisited = null;
        if (goingRight) {
            if (x + getStepDistance() < garden.MAX_X)
                flowerVisited = moveBy(getStepDistance(), 0);
            else {
                goingRight = false;
                flowerVisited = moveBy(0, getStepDistance());
            }
        } else {
            if (x > 0)
                flowerVisited = moveBy(-getStepDistance(), 0);
            else {
                goingRight = true;
                flowerVisited = moveBy(0, getStepDistance());
            }
        }
        // if reach bottom right corner, the bee dies
        if (location.getX() + getStepDistance() >= garden.MAX_X
              && location.getY() + getStepDistance() >= garden.MAX_Y) {
            // the bee's work is done and it dies
            energy = 0;
        }

        return flowerVisited;
    }


}
