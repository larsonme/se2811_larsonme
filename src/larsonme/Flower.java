/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import java.awt.*;

/**
 * A stationary energy source (or drain) for the Bees.
 */
public abstract class Flower extends GardenObject {
    /**
     * Constructs a new Flower within the specified garden.
     * @param garden Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public Flower(Garden garden, Point location) {
        super(garden, location);
    }

    /** Responds to AbstractBaseBee visiting this flower */
    public abstract void visitedBy(AbstractBaseBee b);
}
