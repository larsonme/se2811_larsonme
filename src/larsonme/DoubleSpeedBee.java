/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;


/**
 * This class creates a DoubleSpeedBee which has double the moving distance as a BaseBee.
 */
public class DoubleSpeedBee extends AbstractBeeDecorator {

    /**
     * Constructor for the DoubleSpeedBee object
     * @param bee The Bee that it is a specialized version of.
     */
    public DoubleSpeedBee(BasicBee bee){
        super(bee);
        setDistanceModifier(2);
        bee.setBackgroundColor("LIGHTBLUE");
        backgroundColor = "LIGHTBLUE";
    }


}
