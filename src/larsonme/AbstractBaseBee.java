/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.awt.*;

/**
 * A AbstractBaseBee that seeks nectar from flowers to continue living
 */
public abstract class AbstractBaseBee extends GardenObject implements BasicBee{
    /**
     * The amount of nectar (life) that the AbstractBaseBee has remaining
     */
    protected int energy;
    protected int energyMultiplier = 1;
    protected int shieldStrength = 0;
    protected int distanceModifier = 1;
    private double stepDistance = Garden.STEP_DISTANCE;


    /**
     * Text displayed next to the AbstractBaseBee's icon within the Garden.
     */
    protected Text energyDisplay = new Text("0");

    /**
     * Constructs a new AbstractBaseBee within the specified garden.
     * @param garden Garden in which the AbstractBaseBee lives
     * @param location The AbstractBaseBee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The AbstractBaseBee's initial energy. Will be set to 0 if greater than 0.
     */
    public AbstractBaseBee(Garden garden, Point location, int initialEnergy) {
        super(garden, location);
        setEnergy(initialEnergy);
    }


    /**
     * Sets the AbstractBaseBee's exact energy level.
     * AbstractBaseBee's energy will not go below zero.
     * @param amount New energy level. If negative, energy is set to 0.
     */
    private void setEnergy(int amount) {
        energy = Math.max(0, amount);
    }

    /**
     * Adds or removes the the AbstractBaseBee's energy.
     * AbstractBaseBee's energy will not go below zero.
     * @param delta the amount to increase the energy by (reduce if negative)
     */
    public void changeEnergy(int delta) {
        delta = delta*energyMultiplier;
        while(shieldStrength != 0 && delta != 0){
            delta++;
            shieldStrength--;
        }
        if(delta != 0) {
            setEnergy(energy + delta);
        }
        setEnergyDisplayText();
    }

    public int getEnergy() {
        return energy;
    }

    /**
     * Move to the specified location.
     * (0,0) is the top left corner of the screen.
     * If the specified location is out of bounds, the AbstractBaseBee will
     * be placed within bounds.
     *
     * If the AbstractBaseBee lands on a flower, it will visit that flower.
     *
     * @param newX horizontal coordinate. Positive to the right.
     * @param newY vertical coordinate. Positive downward.
     */
    private Flower moveTo(double newX, double newY) {
        Flower flowerVisited = null;
        changeEnergy(-1);
        location.setLocation(Math.min(garden.MAX_X, Math.max(0, newX)),
                             Math.min(garden.MAX_Y, Math.max(0, newY)));

        Flower closestFlower = garden.closestFlowerTo(location);
        if (onTopOf(closestFlower,getStepDistance())) {
            closestFlower.visitedBy(this);
            flowerVisited = closestFlower;
        }
        return flowerVisited;
    }

    /**
     * Moves the bee relative to its current position.
     * @param deltaX horizontal coordinate of step. Positive to the right
     * @param deltaY vertical coordinate of step. Positive downward
     */
    protected Flower moveBy(double deltaX, double deltaY) {
        return moveTo(location.getX() + deltaX, location.getY() + deltaY);
    }

    /**
     * Calculates how the AbstractBaseBee should move.
     * This method should call moveBy as its last action.
     */
    public abstract Flower step();

    /**
     * Load the icon used to display the AbstractBaseBee within the Garden.
     * @param path Path to icon to load
     */
    @Override
    public void loadImage(String path) {
        Pane beePane = new VBox();
        ImageView beeIV = new ImageView(new Image("file:" + path));
        beeIV.setPreserveRatio(true);
        beeIV.setFitWidth(50.0);
        beePane.getChildren().add(beeIV);
        beePane.getChildren().add(energyDisplay);
        energyDisplay.setFill(Color.BLACK);
        setEnergyDisplayText();
        image = beePane;
    }


    /**
     * Sets the energy Multiplier
     * @param energyMultiplier -1 if it should be reset to 1.  Otherwise just a whole number that scales it
     */
    public void setEnergyMultiplier(int energyMultiplier) {
        if(energyMultiplier != -1){
            this.energyMultiplier= this.energyMultiplier*energyMultiplier;
        }
        else{
            this.energyMultiplier = 1;
        }
        if(this.energyMultiplier>2){
            this.energyMultiplier =2;
        }
    }
    public void setShieldStrength(int shieldStrength) {
        this.shieldStrength = shieldStrength;
        setEnergyDisplayText();

    }
    @Override
    public BasicBee getBee() {
        return this;
    }
    public int getShield() {
        return shieldStrength;
    }

    public double getStepDistance() {
        return stepDistance;
    }

    public void setStepDistance(double stepDistance) {
        this.stepDistance = stepDistance * distanceModifier;
    }

    /**
     * Sets the distance modifier
     * @param distanceModifier -1 if it should be reset to 1.  Otherwise just a whole number that scales it
     */
    public void setDistanceModifier(int distanceModifier) {
        if(distanceModifier != -1){
            this.distanceModifier = this.distanceModifier*distanceModifier;
        }
        else{
            this.distanceModifier = 1;
        }
        if(this.distanceModifier>2){
            this.distanceModifier =2;
        }
        setStepDistance(Garden.STEP_DISTANCE);
    }

    public String getBackgroundColor() {
        return this.backgroundColor;

    }

    /**
     * Sets the text to be shown under the Bee to describe what Adapters are acting on it.
     */
    public void setEnergyDisplayText() {
        String displayText = "E: " + energy;
        if(shieldStrength != 0){
            displayText += "\nS: " + shieldStrength;
        }
        if(energyMultiplier != 1){
            displayText += "\nDE ";
        }

        if(distanceModifier != 1){
            if(energyMultiplier ==1){
                displayText += "\n";
            }
            displayText += "DD";
        }
        energyDisplay.setText(displayText);
    }
}
