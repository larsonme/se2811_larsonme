/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import javafx.scene.Node;


public abstract class AbstractBeeDecorator implements BasicBee{
    BasicBee bee;


    String backgroundColor;

    public AbstractBeeDecorator(BasicBee bee) {
        this.bee = bee;
    }

    public void setBackgroundColor(String color){
        bee.setBackgroundColor(color);
    }


    /**
     * Sets the energy multiplier
     * @param energyMultiplier -1 if it should be reset to 1.  Otherwise just a whole number that scales it
     */
    @Override
    public void setEnergyMultiplier(int energyMultiplier) {
        bee.setEnergyMultiplier(energyMultiplier);
    }

    public Node getImage(){
        return bee.getImage();
    }

    public void showOnPane(){
        bee.showOnPane();
    }

    @Override
    public Flower step() {
        return bee.step();
    }

    @Override
    public int getEnergy() {
        return bee.getEnergy();
    }

    @Override
    public void setShieldStrength(int shieldStrength) {
        bee.setShieldStrength(shieldStrength);
    }
    @Override
    public BasicBee getBee() {
        return bee;
    }
    public void loadImage(String path){
        bee.loadImage(path);
    }

    public int getShield(){
        return bee.getShield();
    }

    /**
     * Calls the same method on the AbstractBaseBee
     * @param distanceModifier -1 if it should be reset to 1.  Otherwise just a whole number that scales it
     */
    public void setDistanceModifier(int distanceModifier) {
        bee.setDistanceModifier(distanceModifier);
    }
    public double getStepDistance(){
        return bee.getStepDistance();
    }

    public String getBackgroundColor() {
        return this.backgroundColor;
    }
    @Override
    public void setEnergyDisplayText() {
        bee.setEnergyDisplayText();
    }

}
