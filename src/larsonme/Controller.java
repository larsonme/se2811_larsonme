/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Controller for the garden window.
 * Layout described in garden.fxml
 */
public class Controller {

    @FXML
    private Pane gardenPane, legendPane;

    private Garden garden = new Garden();

    /**
     * Sets up garden and adds Bees and Flowers.
     */
    @FXML
    public void initialize() {
        gardenPane.setStyle("-fx-background-color: linear-gradient(to bottom right, derive(goldenrod, 20%), derive(goldenrod, -40%));");
        gardenPane.setFocusTraversable(true); // ensure garden pane will receive keypresses
        garden.addRandomBees(4);
        garden.addRandomFlowers(10);
        for(Flower f: garden.getFlowers() ) {
            gardenPane.getChildren().add(f.getImage());
            f.showOnPane();
        }
        for(BasicBee b: garden.getBees()) {
            gardenPane.getChildren().add(b.getImage());
            b.showOnPane();
        }
        setupLegendPane();
    }

    /**
     * Helper method for setupLegendPane
     * @param path path of image to load
     * @return the the loaded image
     */
    private ImageView loadPicture(String path) {
        ImageView i = new ImageView(new Image("file:" + path));
        i.setFitWidth(50.0);
        i.setFitHeight(50.0);
        return i;
    }

    /**
     * Creates simple graphical legend
     */
    private void setupLegendPane() {
        legendPane.getChildren().add(loadPicture("daisy.jpg"));
        legendPane.getChildren().add(new Text("Daisy\nGives the Bee 10 energy."));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("nightshade.jpg"));
        legendPane.getChildren().add(new Text("Venus Bee Trap"));
        legendPane.getChildren().add(new Text("Removes the Bee's \nenhancement when visited." +
                "\nEnergy cost is 3."));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("rose.jpg"));
        legendPane.getChildren().add(new Text("Enhancing Flower"));
        legendPane.getChildren().add(new Text("Adds an enchancement\nwhen visitied.\nEnergy cost is 5"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-1.jpg"));
        legendPane.getChildren().add(new Text("Random Path Bee"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(loadPicture("bee-2.jpg"));
        legendPane.getChildren().add(new Text("Scan Bee"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(createLegendRectangle("LIGHTBLUE"));
        legendPane.getChildren().add(new Text("Bee with Decorator"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(new Text("S: #"));
        legendPane.getChildren().add(new Text("Bee with Shield"));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(new Text("DE"));
        legendPane.getChildren().add(new Text("All energy changes are doubled."));
        legendPane.getChildren().add(new Text(""));
        legendPane.getChildren().add(new Text("DD"));
        legendPane.getChildren().add(new Text("Bee moves twice as fast"));
        legendPane.getChildren().add(new Text(""));



    }

    /**
     * Creates the Rectangle object for each entry that needs it in the Legend
     * @param fillColor The color to fill the rectangle with
     * @return The rectangle object.
     */
    public Rectangle createLegendRectangle(String fillColor){
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(50);
        rectangle.setHeight(50);
        rectangle.setFill(Paint.valueOf(fillColor));
        return rectangle;
    }
    /**
     * Simluates one step forward for all Bees in the game
     * @param event the key pressed. Must be the right arrow for a step to occur.
     */
    @FXML
    public void onKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.RIGHT) {
            for (BasicBee b : garden.getBees()) {
                Flower flowerTypeVisited = b.step();
                    if (flowerTypeVisited instanceof VenusBeeTrap || b.getEnergy()<1) {
                        b = garden.removeDecorator(b);
                        b.setBackgroundColor(b.getBackgroundColor());
                    }
                    else if(flowerTypeVisited instanceof AddDecoratorFlower){
                        b = garden.addDecorator(b);

                    }

                if (b.getEnergy() > 0) {
                    b.showOnPane();
                }
                else{
                    b.setBackgroundColor(b.getBackgroundColor());
                    b.setEnergyDisplayText();
                }

            }
        }
    }
}
