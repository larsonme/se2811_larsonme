/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

/**
 * This class creates a ShieldBee which has a shield which takes damage before energy is lost.
 */
public class ShieldBee extends AbstractBeeDecorator {

    /**
     * This is the Constructor for the ShieldBee class
     * @param bee The BaseBee that it is a specialized version of.
     */
    public ShieldBee(BasicBee bee){
        super(bee);
        bee.setBackgroundColor("LIGHTBLUE");
        bee.setShieldStrength(20);
        this.backgroundColor = "LIGHTBLUE";
    }


}
