/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import java.awt.*;
import java.util.ArrayList;

/**
 * The Garden in which Blowers grow, and Bees buss around looking for flowers.
 */
public class Garden {
    protected ArrayList<Flower> flowers = new ArrayList<>();
    protected ArrayList<BasicBee> bees = new ArrayList<>();
    public final static double STEP_DISTANCE = 22.0;

    /**
     * Maximum distance a AbstractBaseBee may move along horizontal and vertical axes.
     */

    /**
     * Size of garden in STEP_DISTANCEs
     */
    public final static int SQUARES_IN_GARDEN = 10;

    /**
     * Size of garden in pixels
     */
    public final static double MAX_X = (SQUARES_IN_GARDEN - 1) * STEP_DISTANCE * 2,
                               MAX_Y = (SQUARES_IN_GARDEN - 1) * STEP_DISTANCE * 2;

    /**
     * Add flowers to the garden
     * @param numFlowers Number of flowers to add.
     */
    public void addRandomFlowers(int numFlowers) {
        for(int i = 0; i < numFlowers; ++i) {
            Point loc = new Point((int)(Math.random() * MAX_X),
                                  (int)(Math.random() * MAX_Y));
            Flower f;
            double number = Math.random();
            if (number > 0.4) {
                f = new Daisy(this, loc);
                f.loadImage("daisy.jpg");
            } else if(number>0.1 && number<0.4){
                f = new VenusBeeTrap(this, loc);
                f.loadImage("nightshade.jpg");
            }
            else{
                f = new AddDecoratorFlower(this, loc);
                f.loadImage("rose.jpg");
            }
            flowers.add(f);
        }
    }

    /**
     * Add bees to the garden.
     * @param numBees Number of Bees to add.
     */
    public void addRandomBees(int numBees) {
        for(int i = 0; i < numBees; i++) {
            Point loc = new Point((int)(Math.random() * MAX_X),
                                  (int)(Math.random() * MAX_Y));
            BasicBee b;
            if (Math.random() > 0.5) {
                b = new RandomPathBee(this, loc, 20);
                b.loadImage("bee-1.jpg");
            } else {
                b = new ScanBee(this, loc, 200);
                b.loadImage("bee-2.jpg");
            }
            double value = Math.random();
            if(value<0.33){
                b = new ShieldBee(b);
            }
            else if(value > 0.33 && value<0.66){
                b = new DoubleSpeedBee(b);
            }
            else{
                b = new DoubleEnergyBee(b);
            }
            if(value>0.20 && value<0.40){
                b = new DoubleSpeedBee(b);
                bees.add(b);

            }
            else if(value>0.50 && value<0.70){
                b = new DoubleEnergyBee(b);
                bees.add(b);
            }
            else if(value<=0.20){
                b = new DoubleEnergyBee(b);
                bees.add(b);
            }
            else{
                bees.add(b);
            }
            b.setEnergyDisplayText();
        }
    }

    /**
     * Find the Flower closest to a given location.
     *
     * (This method helps Bees find flowers and determine if they should tag a flower.)
     *
     * @param location Point to find flower near.
     * @return The closest flower to location.
     */
    public Flower closestFlowerTo(Point location) {
        double minDistance = Double.MAX_VALUE;
        Flower closestFlower = null;
        for(Flower f : flowers) {
            if (location.distance(f.getLocation()) < minDistance) {
                closestFlower = f;
                minDistance = location.distance(f.getLocation());
            }
        }
        return closestFlower;
    }

    /**
     * Finds a random flower within the Garden.
     * @return The random flower.
     */
    public Flower randomFlower() {
        int flowerIndex = (int)(Math.random() * flowers.size());
        return flowers.get(flowerIndex);
    }

    public ArrayList<Flower> getFlowers() {
        return flowers;
    }

    public ArrayList<BasicBee> getBees() {
        return bees;
    }

    public BasicBee removeDecorator(BasicBee b){
        int index = bees.indexOf(b);
        b = b.getBee();
        bees.set(index,b);
        b.setBackgroundColor("WHITE");
        b.setShieldStrength(0);
        b.setDistanceModifier(-1);
        b.setEnergyMultiplier(-1);
        return b;

    }

    /**
     * This adds a decorator to a Bee when it flies into an AddDecoratorFlower
     * @param b The Bee to add the decorator to.
     */
    public BasicBee addDecorator(BasicBee b){
        int randomNumber = (int)(Math.random()*3);
        if(randomNumber == 0){
            b = new DoubleEnergyBee(b);
        }
        else if(randomNumber == 1){
            b = new DoubleSpeedBee(b);
        }
        else {
            b = new ShieldBee(b);
        }

        return b;
    }
}
