/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;


/**
 * This class creates a DoubleEnergyBee which has double the energy and uses twice the energy as a BaseBee
 */
public class DoubleEnergyBee extends AbstractBeeDecorator {

    /**
     * Constructor for the DoubleEnergyBee object
     * @param bee The Bee that it is a specialized version of.
     */
    public DoubleEnergyBee(BasicBee bee){
        super(bee);
        bee.setBackgroundColor("LIGHTBLUE");
        bee.setEnergyMultiplier(2);
        this.backgroundColor = "LIGHTBLUE";


    }


}
