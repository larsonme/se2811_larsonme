/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;


import java.awt.*;

/**
 * This Flower adds a Decorator to a Bee when visited and takes away 5 energy as a trade off
 */
public class AddDecoratorFlower extends Flower {
    private final int ENERGY_TO_GIVE = -5;
    /**
     * Constructs a new Flower within the specified garden.
     *
     * @param garden   Garden in which the Flower lives
     * @param location The Flower's initial location. Not currently checked if out of bounds.
     */
    public AddDecoratorFlower(Garden garden, Point location) {
        super(garden, location);
    }

    /**
     * Gives the Bee energy when visited.  The adding of the enhancement is handled in the Controller class
     * @param b The Bee that visited this Flower.
     */
    @Override
    public void visitedBy(AbstractBaseBee b) {
        b.changeEnergy(ENERGY_TO_GIVE);
    }
}
