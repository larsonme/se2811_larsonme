/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;

import java.awt.*;

/**
 * A AbstractBaseBee which picks random flower, flies to it, then picks another
 */
public class RandomPathBee extends AbstractBaseBee {
    protected Flower target = null;

    /**
     * @param garden Garden in which the AbstractBaseBee lives
     * @param startingLocation The AbstractBaseBee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The AbstractBaseBee's initial energy. Will be set to 0 if greater than 0.
     */
    public RandomPathBee(Garden garden, Point startingLocation, int initialEnergy) {
        super(garden, startingLocation, initialEnergy);
    }

    /**
     * Picks a random flower and flies on the diagonal closest to it.
     * Moves by Garden.STEP_DISTANCE along each axis. (So total distance along diagonal is sqrt(2)*Garden.STEP_DISTANCE.)
     * Upon reaching the flower, it visits it picks a new flower.
     */
    @Override
    public Flower step() {
        Flower flowerVisited = null;
        if (target == null) {
            target = garden.randomFlower();
        }
        if (onTopOf(target,getStepDistance())) {
            target.visitedBy(this);
            target = null;
        } else {
            double deltaX = Math.copySign(getStepDistance(),
                                          target.getLocation().getX() - location.getX());
            double deltaY = Math.copySign(getStepDistance(),
                                          target.getLocation().getY() - location.getY());
            flowerVisited = moveBy(deltaX, deltaY);
        }
        return flowerVisited;
    }
}
