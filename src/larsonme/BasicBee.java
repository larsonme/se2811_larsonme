/*
 * SE2811
 * Winter 2018
 * Lab 5
 * Name: Michael Larson
 * Created: 1/14/2018
 */
package larsonme;


import javafx.scene.Node;

public interface BasicBee {
    String backgroundColor = "WHITE";
    void setDistanceModifier(int distanceModifier);
    double getStepDistance();

    void loadImage(String path);

    void showOnPane();

    Node getImage();

    Flower step();

    int getEnergy();

    void setBackgroundColor(String color);

    void setEnergyMultiplier(int energyMultiplier);

    void setShieldStrength(int shieldStrength);

    BasicBee getBee();

    int getShield();

    String getBackgroundColor();

    void setEnergyDisplayText();
}